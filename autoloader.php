<?php
$config = include __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
function __autoload($className) {
	$arrPathFile = explode("\\", $className);
	$folderNamespace = strtolower($arrPathFile[0]);
	$classFile = $arrPathFile[1];
	$pathClass = __DIR__ . DIRECTORY_SEPARATOR . $folderNamespace . DIRECTORY_SEPARATOR . $classFile . '.php';
	if (file_exists($pathClass)) {			
		include $pathClass;			
	} else {
		echo "Cannot found class " . $className;
		exit();
	}
}

?>
