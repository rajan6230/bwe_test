<?php
/* database hosting */
$db_host = 'localhost';
/* database name */
$db_name = 'bwe_test';
/* enter database username */
$db_username = 'root';
/* enter database password */
$db_password = '';

$configParams = array(
	'database' => array(
		'dsn' => 'mysql:host=' . $db_host . ';dbname=' . $db_name,
		'db_user' => $db_username,
		'db_pass' => $db_password
	),
);
return $configParams;
?>