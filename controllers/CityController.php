<?php
namespace Controllers;
use Models\City;

class CityController {

	public $city;
	public $data = array();
	public function __construct() {
		$this->city = new City();
	}

	public function createCityAction() {

		$this->city->city_name = $this->data['city_name'];
		$this->city->description = $this->data['description'];
		return $this->city->save();
	}

	public function editCityAction($id) {

		$checkRow = $this->findById($id);
		if ($checkRow === false) {
			return $checkRow;
		} else {
			$this->city->id = $id;
			$this->city->city_name = $this->data['city_name'];
			$this->city->description = $this->data['description'];
			return $this->city->save();
		}
	}	
	public function deleteCityAction($id) {
		$checkRow = $this->findById($id);
		if ($checkRow === false) {
			return $checkRow;
		} else {
			return $this->city->deleteRow($id);
		}
	}

	public function listCityAction() {
		return $this->city->getAll();
	}

}

?>