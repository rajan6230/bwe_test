<?php
namespace Controllers;
use Models\Contact;
use Models\City;

class ContactController {

	public $contact;	
	public $data = array();
	public function __construct() {
		$this->contact = new Contact();
	}
	
	public function createContactAction() {

		$this->contact->name = $this->data['name'];
		$this->contact->first_name = $this->data['first_name'];
		$this->contact->email = $this->data['email'];
		$this->contact->street = $this->data['street'];
		$this->contact->zip_code = $this->data['zip_code'];
		$this->contact->id_city = $this->data['id_city'];
		$this->contact->published = $this->data['published'];
		return $this->contact->save();
	}

	public function editContactAction($id) {

		$checkRow = $this->findById($id);
		if ($checkRow === false) {
			return $checkRow;
		} else {
			$this->contact->id = $id;
			$this->contact->name = $this->data['name'];
			$this->contact->first_name = $this->data['first_name'];
			$this->contact->email = $this->data['email'];
			$this->contact->street = $this->data['street'];
			$this->contact->zip_code = $this->data['zip_code'];
			$this->contact->id_city = $this->data['id_city'];
			return $this->contact->save();
		}
	}	
	public function updateGroupContactAction($id){
		$this->contact->id = $id;
		$this->contact->id_group = $this->data['id_group'];
		
		return $this->contact->updateGroupOfContact();
	}
		
	public function deleteContactAction($id) {
		$checkRow = $this->findById($id);
		if ($checkRow === false) {
			return $checkRow;
		} else {
			return $this->contact->deleteRow($id);
		}
	}
	
	public function listContactAction($order_by = 'name', $sort = 'ASC') {
		return $this->contact->getAll($order_by, $sort);
	}
	
	public function exportContactAction() {
		$contact = $this->contact->getAll('id', 'ASC');
		$domXml = new \DOMDocument();
		$domXml->formatOutput = true;
		$domXml->encoding = 'utf-8';
		$rootXml = $domXml->createElement("address_book");
		$domXml->appendChild($rootXml);
		$simpleXmlElement = simplexml_import_dom($domXml);
		$i = 1;
		foreach ($contact as $add) {
			$contactNode = $simpleXmlElement->addchild("contact_".$i); 
			$contactNode->addChild('id', $add['id']);
			$contactNode->addChild('name', $add['name']);
			$contactNode->addChild('first_name', $add['first_name']);
			$contactNode->addChild('email', $add['email']);
			$contactNode->addChild('street', $add['street']);
			$contactNode->addChild('zip_code', $add['zip_code']);
			$contactNode->addChild('city_name', $add['city_name']);
			$contactNode->addChild('created', $add['created']);
			$contactNode->addChild('modified', $add['modified']);
			$i++;
		}
		return $simpleXmlElement->asXML();		
	}

	public function exportJsonContactAction() {
		$contact = $this->contact->getAll('id', 'ASC');			
		return $contact;
	}
	public function findById($id) {
		return $this->contact->findById($id);
	}

	public function getCities() {
		$cityCtrl = new City();
		return $cityCtrl->getAll();
	}

}

?>