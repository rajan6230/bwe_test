<?php
namespace Controllers;
use Models\Group;
class GroupController {

	public $group;
	public $data = array();
	public function __construct() {
		$this->group = new Group();
	}
	public function createGroupAction() {
		$this->group->id_parent = isset($this->data['id_parent']) ? $this->data['id_parent'] : null;
		$this->group->group_name = $this->data['group_name'];
		$this->group->description = $this->data['description'];
		return $this->group->save();
	}
	public function editGroupAction($id) {
		$this->group->id = $id;
		$this->group->id_parent = $this->data['id_parent'];
		$this->group->group_name = $this->data['group_name'];
		$this->group->description = $this->data['description'];
		return $this->group->save();
	}
	public function getGroupInherits() {
		$groupParent = $this->group->getParentNode();
		return $groupParent;
	}

	public function getContactListAction($id_group) {
		return $this->group->getContactArrayOf($id_group);
	}

	public function getPrivateContactsAction($id_group) {
		return $this->group->getPrivateContactArrayOf($id_group);
	}
	public function getAllGroups() {
		$groups = $this->group->getAll();
		return $groups;
	}
	
	public function getGroupInherited($idGroup) {
		$parentNode = $this->group->getParentNodeById($idGroup);
		return $parentNode;
	}
	public function getGroupById($id) {
		$groups = $this->group->findById($id);
		return $groups;
	}

	public function listGroupsOfContact($idContact) {
		return $this->group->getGroupsOfContact($idContact);
	}

	public function removeContactGroupAction($id_contact, $id_group) {
		$this->group->id = $id_group;
		$this->group->id_contact = $id_contact;
		return $this->group->removeContactGroup();
	}

	public function deleteGroupAction($idGroup) {
		return $this->group->deleteRow($idGroup);
	}

}

?>