<?php 
if(!isset($_GET['id'])){
	header("Location: index.php");
}else{
	require_once 'autoloader.php';
	$contactCtrl = new Controllers\ContactController();
	$id = $_GET['id'];
	$contactCtrl->deleteContactAction($id);
	header("Location: index.php");
}

?>