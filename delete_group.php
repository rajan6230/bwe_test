<?php 
if(!isset($_GET['id'])){
	header("Location: groups.php");
}else{
	require_once 'autoloader.php';
	$groupCtrl = new Controllers\GroupController();
	$id = $_GET['id'];
	$groupCtrl->deleteGroupAction($id);
	header("Location: groups.php");
}

?>