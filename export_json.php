
<?php
require_once 'autoloader.php';
$contactCtrl = new Controllers\ContactController();
 $data =  $contactCtrl->exportJsonContactAction();
 $file = "Address_book_".date('y_m_d').'.json';
 file_put_contents($file, json_encode($data));
 header("Content-type: application/json");
 header('Content-Disposition: attachment; filename="'.basename($file).'"'); 
 header('Content-Length: ' . filesize($file));
 readfile($file);

?>

