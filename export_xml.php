<?php
require_once 'autoloader.php';
$contactCtrl = new Controllers\ContactController();
$filename = "Address_book_".date('y_m_d').'.xml';
header('Content-type: text/xml');
header("Content-Disposition: attachment; filename=\"$filename\"");
echo $contactCtrl->exportContactAction();
exit();
?>
