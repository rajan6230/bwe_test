<?php
namespace Models;
use Models\DbModel;
use Models\ModelInterface;
use \PDO;

class City extends DbModel implements ModelInterface {

	public $conn;
	private $_name = 'cities';
	public $id;
	public $city_name;
	public $description;

	public function __construct() {
		$config = include __DIR__ . DIRECTORY_SEPARATOR . '../config.php';	
		parent::__construct($config);
		$this->conn = parent::connection();
	}
	
	private function _insert() {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'INSERT INTO ' . $this->_name . ' (
														city_name, 
														description														
													) VALUES(
														:city_name, 
														:description														
													)';

			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':city_name', $this->city_name);
			$smtp->bindValue(':description', $this->description);
			$smtp->execute();
			$smtp->closeCursor();
			$result = $this->conn->lastInsertId();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}

	private function _update() {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'UPDATE ' . $this->_name . ' SET city_name = :city_name, 
													description = :description											
													';
			$sql .=' WHERE id=:id';

			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':city_name', $this->city_name);
			$smtp->bindValue(':description', $this->description);
			$smtp->bindValue(':id', $this->id);

			$result = $smtp->execute();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}

	public function save() {
		$result = false;
		if (empty($this->id)) { // insert data
			$result = $this->_insert();
		} else {  // update data
			$result = $this->_update();
		}
		return $result;
	}
	
	public function findById($id) {
		$group = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT * FROM ' . $this->_name . ' AS c	WHERE c.id=:id';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$smtp->execute();
			$group = $smtp->fetch();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $group;
	}

	public function getAll() {
		$groups = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT * FROM ' . $this->_name . ' AS c	ORDER BY c.city_name';
			$smtp = $this->conn->prepare($sql);
			$smtp->execute();
			$groups = $smtp->fetchAll();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $groups;
	}

	public function deleteRow($id) {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = "DELETE FROM ".$this->_name." WHERE id=:id";
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$result = $smtp->execute();
			$smtp->closeCursor();
		}catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}
	
	public function updateField($fieldName, $value, $id) {
		
	}

}

?>