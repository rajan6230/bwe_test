<?php
namespace Models;
use Models\DbModel;
use Models\ModelInterface;
use \PDO;

class Contact extends DbModel implements ModelInterface {

	public $conn;
	private $_name = 'contacts';
	private $_nameCity = 'cities';
	private $_nameContactGroup = 'contacts_groups';
	public $id;
	public $id_group;
	public $name;
	public $first_name;
	public $email;
	public $id_city;
	public $street;
	public $zip_code;
	public $created;
	public $modified;
	public function __construct() {
		$config = include __DIR__ . DIRECTORY_SEPARATOR . '../config.php';
		parent::__construct($config);
		$this->conn = parent::connection();
	}

	private function _insert() {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'INSERT INTO ' . $this->_name . ' (
														id_city, 
														name, 
														first_name,
														email,
														street,
														zip_code,
														created,
														modified
													) VALUES(
														:id_city, 
														:name, 
														:first_name,
														:email,
														:street,
														:zip_code,
														:created,
														:modified
													)';

			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id_city', $this->id_city);
			$smtp->bindValue(':name', $this->name);
			$smtp->bindValue(':first_name', $this->first_name);
			$smtp->bindValue(':email', $this->email);
			$smtp->bindValue(':street', $this->street);
			$smtp->bindValue(':zip_code', $this->zip_code);
			$smtp->bindValue(':created', date('Y-m-d H:i:s'));
			$smtp->bindValue(':modified', date('Y-m-d H:i:s'));
			$smtp->execute();
			$smtp->closeCursor();
			$result = $this->conn->lastInsertId();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}

	private function _update() {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'UPDATE ' . $this->_name . ' SET id_city = :id_city, 
													name = :name, 
													first_name = :first_name,
													email = :email,
													street = :street,
													zip_code = :zip_code,
													modified = :modified
													';
			$sql .=' WHERE id=:id';

			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id_city', $this->id_city);
			$smtp->bindValue(':name', $this->name);
			$smtp->bindValue(':first_name', $this->first_name);
			$smtp->bindValue(':email', $this->email);
			$smtp->bindValue(':street', $this->street);
			$smtp->bindValue(':zip_code', $this->zip_code);
			$smtp->bindValue(':modified', date('Y-m-d H:i:s'));

			$smtp->bindValue(':id', $this->id);

			$result = $smtp->execute();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}

	public function save() {
		$result = false;
		if (empty($this->id)) { 
			$result = $this->_insert();
		} else {  
			$result = $this->_update();
		}
		return $result;
	}

	public function updateGroupOfContact() {
		$result = false;
		if (!empty($this->id_group)) {
			$idgs = $this->id_group;
			if (!empty($this->id)) { 
				$sqlDel = 'DELETE FROM ' . $this->_nameContactGroup . ' WHERE id_contact=' . $this->id;
				$smtp = $this->conn->prepare($sqlDel);
				$deleted = $smtp->execute();
				$smtp->closeCursor();
				if ($deleted) {
					$sql = 'INSERT INTO ' . $this->_nameContactGroup . ' (id_contact, id_group) VALUES ';
					for ($i = 0; $i < count($idgs); $i++) {
						$sql .="($this->id, $idgs[$i])";
						if ($i != count($idgs) - 1) {
							$sql .=',';
						}
					}
					$smtp = $this->conn->prepare($sql);
					$result = $smtp->execute();
					$smtp->closeCursor();
				}
			}
		}
		return $result;
	}

	public function findById($id) {
		$contact = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT
						a.id,
						a.id_city,
						a.name,
						a.first_name,
						a.email,
						a.street,
						a.zip_code,
						a.created,
						a.modified,
						c.city_name,
						c.description
				FROM ' . $this->_name . ' AS a
				INNER JOIN ' . $this->_nameCity . ' AS c ON a.id_city = c.id 
				WHERE a.id=:id';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$smtp->execute();
			$contact = $smtp->fetch();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $contact;
	}

	public function getAll($order_by = 'name', $sort = 'ASC') {
		$contacts = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT
						a.id,
						a.id_city,
						a.name,
						a.first_name,
						a.email,
						a.street,
						a.zip_code,
						a.created,
						a.modified,
						c.city_name,
						c.description
				FROM ' . $this->_name . ' AS a
				INNER JOIN ' . $this->_nameCity . ' AS c ON a.id_city = c.id  
				ORDER BY ' . $order_by . ' ' . $sort . ' ';
			$smtp = $this->conn->prepare($sql);
			$smtp->execute();
			$contacts = $smtp->fetchAll();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $contacts;
	}

	public function deleteRow($id) {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = "DELETE FROM " . $this->_nameContactGroup . " WHERE id_contact=:id";
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$smtp->execute();
			$smtp->closeCursor();
			
			$sql = "DELETE FROM " . $this->_name . " WHERE id=:id";
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$result = $smtp->execute();
			$smtp->closeCursor();
			
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}

	public function updateField($fieldName, $value, $id) {
		
	}

}

?>