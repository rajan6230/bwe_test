<?php
namespace Models;
use \PDO;
class DbModel {

	public $conn = null;
	private $dsn = null;
	private $db_username;
	private $db_password;

	public function __construct($config) {
		$this->dsn = $config['database']['dsn'];
		$this->db_username = $config['database']['db_user'];
		$this->db_password = $config['database']['db_pass'];
	}

	public function connection() {
		try {
			return $this->conn = new \PDO(
					$this->dsn, $this->db_username, $this->db_password, array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
			);
		} catch (\PDOException $e) {
			echo $e->getMessage();
			exit();
		}
	}

}
