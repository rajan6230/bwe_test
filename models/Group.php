<?php
namespace Models;
use Models\DbModel;
use Models\ModelInterface;
use \PDO;

class Group extends DbModel implements ModelInterface {

	public $conn;
	private $_name = 'groups';
	private $_nameGroupRelation = 'group_relations';
	private $_nameContactsGroups = 'contacts_groups';
	public $id;
	public $id_parent;
	public $id_contact;
	public $group_name;
	public $description;

	public function __construct() {
		$config = include __DIR__ . DIRECTORY_SEPARATOR . '../config.php';
		parent::__construct($config);
		$this->conn = parent::connection();
	}

	private function _insert() {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'INSERT INTO ' . $this->_name . ' (
														group_name,
														description
													) VALUES(
														:group_name,
														:description
													)';

			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':group_name', $this->group_name);
			$smtp->bindValue(':description', $this->description);
			$smtp->execute();
			$smtp->closeCursor();
			$idGroup = $this->conn->lastInsertId();
			$idps = $this->id_parent;
			if (!empty($idps)) {
				$sql = 'INSERT INTO ' . $this->_nameGroupRelation . ' (id_group, id_parent) VALUES ';
				for ($i = 0; $i < count($idps); $i++) {
					$sql .="($idGroup, $idps[$i])";
					if ($i != count($idps) - 1) {
						$sql .=',';
					}
				}
				$smtp = $this->conn->prepare($sql);
				$smtp->execute();
				$smtp->closeCursor();
			}

			$result = $idGroup;
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}

	private function _update() {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'UPDATE ' . $this->_name . ' SET group_name = :group_name,
												description = :description ';
			$sql .=' WHERE id=:id';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':group_name', $this->group_name);
			$smtp->bindValue(':description', $this->description);
			$smtp->bindValue(':id', $this->id);
			$result = $smtp->execute();
			$smtp->closeCursor();
			if ($result) { 
				$idGroup = $this->id;				
				$sqlDel = 'DELETE FROM ' . $this->_nameGroupRelation . ' WHERE id_group=' . $this->id;
				$smtp = $this->conn->prepare($sqlDel);
				$deleted = $smtp->execute();
				$smtp->closeCursor();
				if (!empty($this->id_parent)) {
					if ($deleted) {
						$idps = $this->id_parent;
						$sql = 'INSERT INTO ' . $this->_nameGroupRelation . ' (id_group, id_parent) VALUES ';
						for ($i = 0; $i < count($idps); $i++) {
							$sql .="($idGroup, $idps[$i])";
							if ($i != count($idps) - 1) {
								$sql .=',';
							}
						}
						$smtp = $this->conn->prepare($sql);
						$smtp->execute();
						$smtp->closeCursor();
					}
				}
				$result = $idGroup;
			}
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}

	public function save() {
		$result = false;
		if (empty($this->id)) { 
			$result = $this->_insert();
		} else {  
			$result = $this->_update();
		}
		return $result;
	}

	public function findById($id) {
		$group = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT * FROM ' . $this->_name . ' AS g	WHERE g.id=:id';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$smtp->execute();
			$group = $smtp->fetch();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $group;
	}

	public function getAll() {
		$groups = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT * FROM ' . $this->_name . ' AS g	ORDER BY g.group_name';
			$smtp = $this->conn->prepare($sql);
			$smtp->execute();
			$groups = $smtp->fetchAll();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $groups;
	}

	public function getParentNode() {
		$parentList = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT g1.id, g1.group_name, g2.id as id_parent, g2.group_name as parent_name 
					FROM ' . $this->_name . ' as g1, ' . $this->_name . ' as g2, ' . $this->_nameGroupRelation . ' as r
					WHERE r.id_group = g1.id and r.id_parent=g2.id';

			$smtp = $this->conn->prepare($sql);
			$smtp->execute();
			$parentList = $smtp->fetchAll();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $parentList;
	}

	public function getParentNodeById($id) {
		$parentList = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT g1.id, g1.group_name, g2.id as id_parent, g2.group_name as parent_name 
					FROM ' . $this->_name . ' as g1, ' . $this->_name . ' as g2, ' . $this->_nameGroupRelation . ' as r
					WHERE r.id_group = g1.id and r.id_parent=g2.id AND r.id_group=:id';

			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$smtp->execute();
			$parentList = $smtp->fetchAll();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $parentList;
	}

	public function deleteRow($id) {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'DELETE FROM ' . $this->_nameGroupRelation . '
				WHERE id_group=:id OR id_parent=:id';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$smtp->execute();
			$smtp->closeCursor();
			$sql = 'DELETE FROM ' . $this->_nameContactsGroups . ' WHERE id_group=:id ';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$smtp->execute();
			$smtp->closeCursor();
			$sql = 'DELETE FROM ' . $this->_name . ' WHERE id=:id ';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id', $id);
			$result = $smtp->execute();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}

	public function removeContactGroup() {
		$idContact = $this->id_contact;
		$idGroup = $this->id;
		$result = false;
		if (!empty($idContact) && !empty($idGroup)) {
			$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			try {
				$sql = 'DELETE FROM ' . $this->_nameContactsGroups .
						' WHERE id_contact=' . $idContact .
						' AND id_group=' . $idGroup;
				$smtp = $this->conn->prepare($sql);
				$result = $smtp->execute();
				$smtp->closeCursor();
			} catch (\PDOException $ex) {
				error_log($ex->getMessage());
			}
		}
		return $result;
	}

	public function getParentArray($idGroup) {
		$result = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT g2.id, g2.group_name 
				FROM ' . $this->_name . ' as g1,  ' . $this->_name . ' as g2, ' . $this->_nameGroupRelation . ' as r 
				WHERE g1.id = r.id_parent 
				AND g2.id=r.id_parent 
				AND r.id_group=:id_group';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id_group', $idGroup);
			$smtp->execute();
			$result = $smtp->fetchAll();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $result;
	}

	public function getPrivateContactArrayOf($idGroup) {
		$contacts = array();
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT c.*, ct.city_name 
				FROM contacts as c, cities as ct ,' . $this->_nameContactsGroups . ' as cg
				WHERE c.id = cg.id_contact 
				AND ct.id=c.id_city 
				AND cg.id_group =:id_group';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id_group', $idGroup);
			$smtp->execute();
			$contacts = $smtp->fetchAll();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $contacts;
	}

	public function getContactArrayOf($idGroup) {
		$contactList = array();
		$groupList = array($idGroup);
		$this->getGroupIdList($groupList, $idGroup);
		foreach ($groupList as $idg) {
			$tmps = $this->getPrivateContactArrayOf($idg);
			foreach ($tmps as $tmp) {
				if (!in_array($tmp, $contactList)) {
					$contactList[] = $tmp;
				}
			}
		}
		return $contactList;
	}

	public function getGroupIdList(&$groupList, $idGroup) {
		$arrParents = $this->getParentArray($idGroup);
		foreach ($arrParents as $parent) {
			if (!in_array($parent['id'], $groupList)) {
				$groupList[] = $parent['id'];
				$this->getGroupIdList($groupList, $parent['id']);
			}
		}
	}

	public function getGroupsOfContact($idContact) {
		$groups = false;
		$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		try {
			$sql = 'SELECT g.id, g.group_name, cg.id_contact 
					FROM ' . $this->_name . ' as g, contacts as c, ' . $this->_nameContactsGroups . ' as cg
					WHERE cg.id_group = g.id 
						AND cg.id_contact = c.id 
						AND cg.id_contact =:id_contact';
			$smtp = $this->conn->prepare($sql);
			$smtp->bindValue(':id_contact', $idContact);
			$smtp->execute();
			$groups = $smtp->fetchAll();
			$smtp->closeCursor();
		} catch (\PDOException $ex) {
			error_log($ex->getMessage());
		}
		return $groups;
	}

	public function updateField($fieldName, $value, $id) {
		
	}

}
