<?php
namespace Models;
interface ModelInterface {

	public function save();
	public function findById($id);
	public function getAll();
	public function deleteRow($id);
	
	public function updateField($fieldName, $value, $id);
}

?>
