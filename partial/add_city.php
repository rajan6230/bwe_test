<?php

require_once '../autoloader.php';
$city = new Controllers\CityController();
if(!empty($_POST)){
	$response = array('lastId' => 0, 'status'=>false, 'message'=>'Error: cannot add city!');
	$city->data = $_POST;
	$result = $city->createCityAction();
	if($result!==false){
		$response['lastId'] = $result;
		$response['status'] = true;
		$response['message'] = 'The city is added';		
	}
	echo json_encode($response);
	exit();
}else{
	header("Location: ../index.php");
}

?>
