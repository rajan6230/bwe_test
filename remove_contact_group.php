<?php 
if(!isset($_GET['id_contact']) && !isset($_GET['id_group'])){
	header("Location: groups.php");
}else{
	require_once 'autoloader.php';
	$contactCtrl = new Controllers\GroupController();
	$id_contact = $_GET['id_contact'];
	$id_group = $_GET['id_group'];
	$contactCtrl->removeContactGroupAction($id_contact, $id_group);
	header("Location: view_group_contacts.php?id_group=$id_group");
}

?>